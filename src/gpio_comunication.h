#include <bcm2835.h>
#include <stdio.h>

#ifndef GPIO_COMUNICATION
#define GPIO_COMUNICATION
#define L1 RPI_GPIO_P1_11
#define L2 RPI_GPIO_P1_12
#define L3 RPI_V2_GPIO_P1_13
#define L4 RPI_V2_GPIO_P1_15
#define A1 RPI_V2_GPIO_P1_16  
#define A2 RPI_V2_GPIO_P1_18
#define SS RPI_GPIO_P1_22
#define SC RPI_V2_GPIO_P1_37
#define PC RPI_V2_GPIO_P1_29 
#define JC RPI_V2_GPIO_P1_31
#define PS RPI_V2_GPIO_P1_32
#define JS RPI_V2_GPIO_P1_36
#define JQ1 RPI_V2_GPIO_P1_38
#define JQ2 RPI_V2_GPIO_P1_40

void init_gpio();
int * gpio_verificar();
void turn_on_off(int pos);
#endif
