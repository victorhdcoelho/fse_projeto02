import sys
import threading
from server import (
        Server,
        menu,
        get_temp_and_status,
        )

if __name__ == "__main__":
    addr = sys.argv[1]
    port = int(sys.argv[2])
    server = Server(addr, port)
    server.create_socket()
    server.connect_to_socket(addr, port)
    functions = [get_temp_and_status, menu]
    thrs = []
    for function in functions:
        thrs.append(threading.Thread(target=function, args=(server,)))

    for thr in thrs:
        thr.start()

    for thr in thrs:
        thr.join()

    server.sock.close()
