import os
import socket
import json
import time
import subprocess

# Variaveis globais para ser compartilhada pelas threads
HUM = 0.0
TEMP = 0.0
TEMP_T = "Não informada !"
NEW_TEMP = 0
STOP = 0
STATUS = []

CHOICES = {
            "1": "Pegar temperatura",
            "2": "Lampada 01",
            "3": "Lampada 02",
            "4": "Lampada 03",
            "5": "Lampada 04",
            "6": "GPIO verificar",
            "7": "Ligar Desligar ar",
        }


class Server:
    """ Classe para se conectar ao servidor !"""
    def __init__(self, addr, port):
        self.addr = addr
        self.port = port
        self.sock = None

    def create_socket(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect_to_socket(self, addr, port):
        self.sock.connect((addr, port))

    def send_msg(self, msg):
        msg = msg.encode()
        sended = self.sock.send(msg)
        if sended == 0:
            print("Error in send msg !")

    def receive_msg(self):
        global HUM
        global TEMP
        global STATUS

        rcv_msg = self.sock.recv(70)
        if not rcv_msg:
            return False
        msg = rcv_msg.decode()
        command = msg.split("-")[1]
        msg = msg.split("-")[0]
        # Caso a mensagem recebida seja de atualização
        if command == "1":
            temp_json = json.loads(msg.replace("'", '"'))
            TEMP = temp_json["TEMP"]
            HUM = temp_json["HUM"]
        # Caso a mensagem recebida seja de atualização do status de GPIOS
        if command == "60" or command == "61":
            temp_json = json.loads(msg.replace("'", '"'))
            STATUS = temp_json["status"]
            if command[1] == "1":
                alarm = subprocess.Popen(["omxplayer", "alarme.mp3"],
                                         stdout=subprocess.PIPE,
                                         stdin=subprocess.PIPE)
                alarm.wait()
        return True


def menu(server):
    global STOP
    global TEMP
    global HUM
    global STATUS
    global TEMP_T
    global NEW_TEMP
    while True:
        print("TEMP: {}\t\t\tHUM: {}\t\t\tDESEJADA: {}\n1-Atualizar UI\t2-Lampada 01\n3-Lampada 02\t4-Lampada 03\n5-Lampada 04\tt-Colocar temp\n7- Desligar/Ligar ar\t0-Desligar\n{}".format(TEMP, HUM, TEMP_T, STATUS))
        choice = input()
        if choice == 't':
            TEMP_T = float(input("TEMP: "))
            NEW_TEMP = 1
            # Para atualizar a interface
            choice = '1'
        try:
            command = CHOICES[choice]
        except Exception:
            print("Desligando !")
            STOP = 1
            break
        server.send_msg(command)
        msg = server.receive_msg()
        if msg is False:
            print("Mensagem não recebida !!")
        os.system("clear")


def get_temp_and_status(server):
    global STOP
    global TEMP
    global TEMP_T
    global STATUS
    ligar = 0
    while STOP != 1:
        time.sleep(1)
        # Pega temperatura de um em um segundo
        server.send_msg(CHOICES['1'])
        server.receive_msg()
        # Caso a temperatura seja informada pelo usuário
        if NEW_TEMP == 1:
            if TEMP > TEMP_T and ligar == 0:
                server.send_msg(CHOICES['7'])
                ligar = 1
            elif TEMP <= TEMP_T and ligar == 1:
                server.send_msg(CHOICES['7'])
                ligar = 0
        # Pega o status de todos componentes
        server.send_msg(CHOICES['6'])
        msg = server.receive_msg()
        if msg is False:
            print("Mensagem não recebida !!")
