#include "csv_utils.h"

void init_csv()
{
	FILE * csv = fopen("data.csv", "w");
	fprintf(csv, "data, comando\n");
	fclose(csv);
}

void insert_csv(char * comando)
{
	FILE * csv = fopen("data.csv", "a");
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);
	char date[64];
	assert(strftime(date, sizeof(date), "%c", tm));
	fprintf(csv, "%s,%s\n", date, comando);
	fclose(csv);
}
