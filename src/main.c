#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "i2c_comunication.h"
#include "gpio_comunication.h"
#include "csv_utils.h"
#include "commands.h"
#define BUFFER_LEN 70

void client_comunication(int socket_cliente);
int prepare_client_server(unsigned short servidorPorta);


int main(int argc, char *argv[]) {
	int socketCliente;
	int servidorSocket;
	unsigned short servidorPorta;
	unsigned int clienteLength;
	struct sockaddr_in clienteAddr;
	init_gpio();
	init_csv();

	if (argc != 2) {
		printf("Uso: %s <Porta>\n", argv[0]);
		exit(1);
	}

	servidorPorta = atoi(argv[1]);
	servidorSocket = prepare_client_server(servidorPorta);
	
	while(1) {
		clienteLength = sizeof(clienteAddr);
		if((socketCliente = accept(servidorSocket,
			                      (struct sockaddr *) &clienteAddr,
			                      &clienteLength)) < 0)
			printf("Falha no Accept\n");

		printf("Conexão do Cliente %s\n", inet_ntoa(clienteAddr.sin_addr));

		ClientComunication(socketCliente);
		close(socketCliente);

	}
	close(servidorSocket);
	return 0;
}

void ClientComunication(int socketCliente) {
	char command[BUFFER_LEN] = {'\0'};
	char callback[BUFFER_LEN] = {'\0'};
	int msg_len;

	if((msg_len = recv(socketCliente, command, BUFFER_LEN, 0)) < 0)
		printf("Erro no recv()\n");
	while(msg_len > 0)
	{
		// Comandos
		printf("Msg: %s, Len: %d\n", command, msg_len);	
		execute_commands(command, callback);

		if(send(socketCliente, callback, strlen(callback), 0) == -1)
			printf("Erro no send()\n");
		// Zerar Buffers
		for(int i = 0; i < BUFFER_LEN; i++)
		{
			callback[i] = '\0';
			command[i] = '\0';
		}
		// Receber msg
		if((msg_len = recv(socketCliente, command, BUFFER_LEN, 0)) < 0)
			printf("Erro no recv()\n");
	}
}

int prepare_client_server(unsigned short servidorPorta)
{
	int servidorSocket;
	struct sockaddr_in servidorAddr;

	// Abrir Socket
	if((servidorSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		printf("falha no socker do Servidor\n");

	// Montar a estrutura sockaddr_in
	memset(&servidorAddr, 0, sizeof(servidorAddr)); // Zerando a estrutura de dados
	servidorAddr.sin_family = AF_INET;
	servidorAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servidorAddr.sin_port = htons(servidorPorta);

	// Bind
	if(bind(servidorSocket, (struct sockaddr *) &servidorAddr, sizeof(servidorAddr)) < 0)
		printf("Falha no Bind\n");

	// Listen
	if(listen(servidorSocket, 10) < 0)
		printf("Falha no Listen\n");

	return servidorSocket;
}
