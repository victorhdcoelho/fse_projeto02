#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "i2c_comunication.h"
#include "gpio_comunication.h"
#include "csv_utils.h"

#ifndef COMMANDS
#define COMMANDS


void execute_commands(char * command, char * callback);

#endif

