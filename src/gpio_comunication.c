#include "gpio_comunication.h"
#include <bcm2835.h>
#include <stdio.h>

void init_gpio()
{
	int gpios[14] = {L1, L2, L3, L4, A1, A2, SS, SC, PC, JC, PS, JS, JQ1, JQ2};
	if(!bcm2835_init())
	{
		printf("Error ao iniciar gpio\n");
	}

	for(int i=0; i < 14; i++)
	{
		bcm2835_gpio_fsel(gpios[i], BCM2835_GPIO_FSEL_OUTP);
		bcm2835_gpio_write(gpios[i], LOW);
	}
}

int * gpio_verificar()
{
	int gpios[14] = {L1, L2, L3, L4, A1, A2, SS, SC, PC, JC, PS, JS, JQ1, JQ2};
	static int status[14] = {0};
	
	for(int i=0; i<14; i++)
	{
		status[i] = bcm2835_gpio_lev(gpios[i]);
	}

	return status;
}
void turn_on_off(int pos)
{
	int gpios[14] = {L1, L2, L3, L4, A1, A2, SS, SC, PC, JC, PS, JS, JQ1, JQ2};
	if ( bcm2835_gpio_lev(gpios[pos]))
	{
		bcm2835_gpio_write(gpios[pos], LOW);
	}
	else
	{	
		bcm2835_gpio_write(gpios[pos], HIGH);
	}
}
