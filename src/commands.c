#include "commands.h"


void execute_commands(char * command, char * callback)
{
		int alarm = 0;
		if(!strcmp(command, "Pegar temperatura"))
        {
            temp_humity result = read_te("/dev/i2c-1");
            sprintf(callback, "{'TEMP': %.2f, 'HUM': %.2f}-1", result.temp, result.hum);
            insert_csv(command);
            printf("%s\n", command);
        }
        if(!strcmp(command, "Lampada 01"))
        {
            strcat(callback, "Lampada 01-2");
            turn_on_off(0);
            insert_csv(command);
            printf("%s\n", command);
        }
        if(!strcmp(command, "Lampada 02"))
        {
            strcat(callback, "Lampada 02-3");
            turn_on_off(1);
            insert_csv(command);
            printf("%s\n", command);
        }
        if(!strcmp(command, "Lampada 03"))
        {
            strcat(callback, "Lampada 03-4");
            turn_on_off(2);
            insert_csv(command);
            printf("%s\n", command);
        }
        if(!strcmp(command, "Lampada 04"))
        {
            strcat(callback, "Lampada 04-5");
            turn_on_off(3);
            insert_csv(command);
            printf("%s\n", command);
        }
        if(!strcmp(command, "Ligar Desligar ar"))
        {
            strcat(callback, "Ligar Desligar ar-7");
            turn_on_off(4);
            turn_on_off(5);
            insert_csv(command);
            printf("%s\n", command);
        }

        if(!strcmp(command, "GPIO verificar"))
        {
            int * status = gpio_verificar();

            for(int i=6; i < 14; i++)
            {
                if(status[i] == 1)
                {
                    alarm = 1;
                    break;
                }
                else
                {
                    alarm = 0;
                }
            }
            sprintf(callback, "{'status': [%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d]}-6%d",
                    status[0], status[1], status[2], status[3], status[4], status[5], status[6],
                    status[7], status[8], status[9], status[10], status[11], status[12], status[13], alarm);
            insert_csv(command);
            printf("%s\n", command);
            printf("%s\n", callback);
        }
}
