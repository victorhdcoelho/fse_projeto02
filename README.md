## Projeto de FSE 02

## Como rodar
1. Clone o repositório nos dois servidores(central e destribuído);
2. No servidor distribuído execute `make && ./bin/bin 10115`
3. No servidor do cliente execute `python3 src/cliente.py "192.168.0.52" 10115`

## Em execução:
* **Opção 1** para atualizar o UI (não conseguir fazer o refresh com o curses);
* **Opção 2-5** ligam as lampadas 1, 2, 3, 4 respectivamente. Obs: Lembre de atualizar a UI após a
operação;
* **Opção 6** verificar todos os estados da GPIO, porém como é feito a cada 1 segundo não está explicita
nas opções;
* **Opção 7** Ligar e Desligar os ar condicionados caso estejem ligados ele desliga e caso ligados ele
liga;
* **Opção t** - Colocar a temperatura desejada para iniciar o sistema de On/Off da temperatura;
* **Opção 0** - Desligar o sistema;
* Caso algum sensor venha a ser acionados os sensores são a posição 6-13 o alarme vai tocar em aproximadament 1 segundo ou 2 segundos após o acionamento;

**OBS: Para que saiba o status do sistema sempre aperte 1 para atualizar a UI**

## Tabela de posições   

|Descrição|Posição|
|---|---|
|lampada 01| 0|
|lampada 02| 1|
|lampada 03| 2|
|lampada 04| 3|
|ar 01| 4|
|ar 02| 5|
|sensor presença 01| 6|
|sensor presença 02| 7|
|sensor abertura 01| 8|
|sensor abertura 02| 9|
|sensor abertura 03| 10|
|sensor abertura 04| 11|
|sensor abertura 05| 12|
|sensor abertura 06| 13|

**OBS: O vetor de gpio sempre ficará embaixo do menu !!** 
